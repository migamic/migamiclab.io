baseURL: "https://migamic.gitlab.io/"
languageCode: "en-us"
title: "Jaume Ros"
theme: hugo-profile

outputs:
  home:
    - "HTML"
    - "RSS"
    - "JSON"
  page:
    - "HTML"
    - "RSS"

Pagination.pagerSize: 3
enableRobotsTXT: true
# disqusShortname: your-disqus-shortname
# googleAnalytics: G-MEASUREMENT_ID

markup:
  goldmark:
    renderer:
      unsafe: true

Menus:
  main:
    #- identifier: projects 
    #  name: Project Gallery
    #  title: Project Gallery
    #  url: /projects
    #  weight: 1
    #- identifier: blog
    #  name: Blog
    #  title: Blog posts
    #  url: /blogs
    #  weight: 2
    #- identifier: misc
    #  name: Misc
    #  title: Miscellaneous
    #  url: /misc
    #  weight: 3
    #Dropdown menu
    - identifier: other 
      title: Other
      name: Other
      weight: 3
    - identifier: gallery
      title: Project Gallery
      name: Project Gallery
      url: /projects
      parent: other
      weight: 1
    - identifier: misc
      title: Misc
      name: Miscellaneous
      url: /misc
      parent: other
      weight: 2

params:
  title: "Jaume Ros"
  description: Personal website & portfolio
  # staticPath: ""  # The path to serve the static files from
  favicon: "/fav.png"

  # Whether to serve bootstrap css and js files from CDN or not. Can be set to true, "css" or "js" to choose between
  # serving both, only the css, or only the js files through the CDN. Any other value will make so that CDN is not used.
  # Note the lack of "" in true, it should be of boolean type.
  useBootstrapCDN: false

  # If you want to load dynamically responsive images from Cloudinary
  # This requires your images to be uploaded + hosted on Cloudinary
  # Uncomment and change YOUR_CLOUD_NAME to the Cloud Name in your Cloudinary console
  # cloudinary_cloud_name: "YOUR_CLOUD_NAME"
  
  # Whether to add mathjax support on all pages. Alternatively, you can opt-in per page by adding `mathjax: true` in the frontmatter.
  mathjax: false

  # Whether the fade animations on the home page will be enabled
  animate: true

  theme:
    # disableThemeToggle: true
    defaultTheme: "light" # dark

  font:
    fontSize: 1rem # default: 1rem
    fontWeight: 400 # default: 400
    lineHeight: 1.5 # default: 1.5
    textAlign: left # default: left

  # color preference
  color:
  #   textColor:
  #   secondaryTextColor:
  #   backgroundColor:
  #   secondaryBackgroundColor:
    primaryColor: "#35847f"
  #   secondaryColor:

    darkmode:
  #     textColor:
  #     secondaryTextColor:
  #     backgroundColor:
  #     secondaryBackgroundColor:
      primaryColor: "#6fcec8"
  #     secondaryColor:

  # If you want to customize the menu, you can change it here
  navbar:
    align: ms-auto # Left: ms-auto | center: mx-auto | right: me-auto | Default: ms-auto
    # brandLogo: "/logo.png" # Logo for the brand | default is the favicon variable
    showBrandLogo: true # Show brand logo in nav bar | default is true
    brandName: "Jaume Ros" # Brand name for the brand | default is the title variable
    disableSearch: true
    # searchPlaceholder: "Search"
    stickyNavBar:
      enable : true
      showOnScrollUp : true
    menus:
      disableAbout: false
      disableProjects: false
      disableExperience: false
      disableEducation: false
      disableAchievements: false
      disableContact: false

  # Hero
  hero:
    enable: true
    intro: "Hi! I'm"
    title: "Jaume Ros"
    subtitle: "Data scientist and engineer, specializing in computer graphics."
    content: "Welcome to my website :)"
    image: /images/profile.png
    bottomImage:
      enable: true
    # roundImage: true # Make hero image circular | default false
    button:
      enable: false
      name: "Resume"
      url: "#"
      download: true
      newPage: false
    socialLinks:
      fontAwesomeIcons:
        - icon: fab fa-gitlab
          url: https://gitlab.com/migamic
        - icon: fab fa-github
          url: https://github.com/migamic
        - icon: fab fa-orcid
          url: https://orcid.org/0009-0003-9288-843X
        - icon: fab fa-linkedin
          url: https://www.linkedin.com/in/jaume-ros-alonso/
        - icon: fas fa-envelope
          url: mailto:jaume.ros.alo@gmail.com

  # About
  about:
    enable: true
    title: "About Me"
    image: "/images/profile2.jpg"
    content: |-
      I am a data scientist and engineer with a strong background in mathematics and computer graphics.
      I'm currently a PhD student at the Eindhoven University of Technology, researching into the field of Prescrictive Visual Analytics.
    skills:
      enable: true
      title: "These are some of my skills:"
      items:
        - "**Data science**: Python, R"
        - "**Machine learning**: PyTorch, OpenCV, Scikit"
        - "**Computer graphics**: Blender, Unity, OpenGL"
        - "**Linux**: Yes, I use Vim and I'll choose anything scriptable over the prettiest GUI"
        - "**High-performance computing**: C++, OpenMP, CUDA"
        - "**Web development** (mostly frontend)**:** React, Tailwind CSS, Astro"


  # Top projects
  projects:
    enable: true
    title: "Featured projects"
    menutitle: "Projects"
    items: [pixinwav, senda, virtuwheel, honeywords, proceduralcity, memestemplates]
    outro: "Check out the full list of projects in the [Project Gallery](/projects)."

  # Experience
  experience:
    enable: true
    # title: "Custom Name"
    items:
      - job: "PhD student"
        company: "TUe"
        companyUrl: "https://www.tue.nl/"
        date: "Sep 2024 - Present"
        info:
          enable: false
        featuredLink:
          enable: false
        content: "Currently pursuing my PhD at the Visualization Cluster of the Eindhoven University of Technology, in the Netherlands. Doing research in the field of Prescrivtive Visual Analytics."
      - job: "Assistant Researcher"
        company: "ViRVIG"
        companyUrl: "https://www.virvig.eu"
        date: "Feb 2024 - July 2024"
        info:
          enable: true
          content: I'm working part-time while developing my Master's thesis.
        featuredLink:
          enable: true
          name: "Project website"
          url: "https://www.virvig.eu/senda/senda.html"
        content: "I was a part-time assistant researcher at the Research Center for Visualization, Virtual Reality and Graphics Interaction. Working in the SENDA project, where the goal is analyze the impact of human hiking activity on the natural environment, the task I worked on (also my Master's thesis) consisted in the creation of realistic hiking trail networks from Digital Elevation Models."

      - job: "Computer Science Trainee"
        company: "Technica Electronics"
        companyUrl: "https://www.technica-engineering.com/"
        date: "Sep 2022 - Jan 2024"
        info:
          enable: true
          content: I worked part-time while coursing my first 3 semesters of the Master's degree.
        content: |
          I successfully completed a 900h intership and received a job offer to continue developing my work.
          - Main developer of a real-time viewer for compressed image data encoded in Ethernet packets.
          - Assistant to the web team for the development of hardware traceability apps.
          - Built BI interactive dashboards to monitor client orders and hardware traceability.

      - job: "Collaborating Student"
        company: "UPC - TSC"
        companyUrl: "https://tsc.upc.edu"
        date: "Aug 2022 - Jul 2023"
        info:
          enable: true
          content: Granted as a scholarship by the Spanish Minstry of Education.
        content: |
          I collaborated with the Signal Theory and Communications Department from the [Polytechnic University of Catalonia](https://www.upc.edu) to implement several Reinforcement Learning algorithms, for didactic purposes and use on future research projects.

      - job: "Collaborating Student"
        company: "ZeClinics"
        companyUrl: "https://www.zeclinics.com"
        date: "Jan 2021 - Jun 2021"
        info:
          enable: false
        content: |
           My colleagues and I implementated a specialized image processing program to automatically analyze and diagnose Zebrafish larvae. Using low-level Python modules for file processing, scikit-learn and other Machine Learning modules for segmentation, PyTorch for classification through Deep Learning, and Vue.js and Flask for building the Graphical User Interface.

           My main tasks were in the development of the Python backbone for image and video processing and implementing the low-level real-time segmentation algorithms.


  # Education
  education:
    enable: true
    # title: "Custom Name"
    index: false
    items:
      - title: "Master's degree in Innovation and Research in Informatics"
        school:
          name: "Universitat Politècnica de Catalunya"
          url: "https://www.upc.edu"
        date: "2022-2024"
        GPA: "9.33 out of 10 (2nd of my class)"
        featuredLink:
          enable: true
          name: "Written thesis"
          url: "https://upcommons.upc.edu/handle/2117/416532"
        content: |-
          A 2-year master’s degree in the [Barcelona School of Informatics](https://www.fib.upc.edu). Taking the specialization in Computer Graphics and Virtual Reality.

          My master's thesis about the procedural generation of hiking trail networks obtained a grade of 10/10 with honors.
      - title: "Bachelor's thesis"
        school:
          name: "Dublin City University"
          url: "https://www.dcu.ie/"
        date: "2022"
        GPA: "9.2 out of 10"
        featuredLink:
          enable: true
          name: "Written thesis"
          url: "https://upcommons.upc.edu/handle/2117/375117"
        content: |-
          Final thesis on Deep Multimodal Steganography developed at [Dublin City University](https://www.dcu.ie/).
      - title: "Bachelor's degree in Data Science and Engineering"
        school:
          name: "Universitat Politècnica de Catalunya"
          url: "https://www.upc.edu"
        date: "2018-2022"
        GPA: "8.92 out of 10 (top 5 of my class)"
        content: |-
          A 4-year bachelor’s degree jointly taught in the [Barcelona School of Informatics](https://www.fib.upc.edu), the [Barcelona School of Telecommunications and Engineering](https://etseib.upc.edu) and the [School of Mathematics and Statistics](https://fme.upc.edu).

  # Achievements
  achievements:
    enable: true
    title: "Awards"
    items:
      - title: Second place in LauzHack
        content: Our team finished second in the general classification for the 2023 LauzHack hackathon, in the École Polytechnique Fédérale de Lausanne, Switzerland.
        url: "https://2023.lauzhack.com/"
        date: 2023
      - title: Intel prize for outstanding performance in Mobile Robotics
        content: I received this prize from Intel Ireland (550€ + diploma) for winning the student robotics competition from the Mobile Robotics course in Dublin City University.
        url: "https://www.dcu.ie/engineeringandcomputing/news/2022/oct/14-dcu-students-selected-receive-intel-undergraduate-awards"
        date: 2022
      - title: Recognition for the university entrance exams (PAU)
        content: I received this distinction from the Catalan Government for obtaining a grade higher or equal than 9 (9.5 out of 10) in my university entrance exams.
        url: "https://recercaiuniversitats.gencat.cat/en/01_departament_recerca_i_universitats/premis_i_reconeixements/distincions_pau/"
        date: 2017


  #Contact
  contact:
    enable: true
    # title: "Custom Name"
    content: Found a bug in a project? Have a question? Just want to say hi? My inbox is always open!
    btnName: Email me
    btnLink: mailto:jaume.ros.alo@gmail.com
    # formspree:
    #   enable: true # `contact.email` value will be ignored
    #   formId: abcdefgh # Take it from your form's endpoint, like 'https://formspree.io/f/abcdefgh'
    #   emailCaption: "Enter your email address"
    #   messageCaption: "Enter your message here"

  footer:
    recentPosts:
      path: "blogs"
      count: 3
      title: Recent Posts
      enable: false
      disableFeaturedImage: false

  # List pages like blogs and posts
  listPages:
    disableFeaturedImage: false

  # Single pages like blog and post
  singlePages:
    readTime:
      enable: false
      content: "min read"
    scrollprogress:
      enable: true

  # For translations
  terms:
    read: "Read"
    toc: "Table Of Contents"
    copyright: "All rights reserved"
    pageNotFound: "Page not found"
    emailText: "Check out this site"

  datesFormat:
    article: "Jan 2, 2006"
    articleList: "Jan 2, 2006"
    articleRecent: "Jan 2, 2006"

  #customScripts: -| # You can add custom scripts which will be added before </body> tag
  #  <script type="text/javascript"><!-- any script here --></script>
