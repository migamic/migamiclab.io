---
title: "Miscellaneous :artist_palette:"
draft: false
layout: "misc"
sections:
  youtubeVideos:
    title: "3D animation"
    description: 'I''ve always loved the world of 3D animation. I gave it a go with Autodesk Maya when I was 13, and a few years later switched to Blender. Here''s a collection of some of my more cinematic projects that ended up on YouTube.'
    videos:
        - yt_id: "2Z6Vr5oOvMQ"
          title: "H2BOT Showcase"
          date: 2024-05-29
          description: 'A 3D animation showcasing the design of a robot concept developed in a university course. The whole video was developed in a single week (from the first idea to the rendering).'
          software: blender
        - yt_id: "vodSuyKAkf8"
          title: "Meteorite"
          date: 2022-07-01
          description: 'A short summer project, where I strived for realism and went for a phone''s camera look, both in movement and image quality. Fluids and particles also play a big role in this scene.'
          software: blender
        - yt_id: "OQHzKnRDKPk"
          title: "CGI Compilation"
          date: 2021-09-19
          description: 'A compilation of different CGI/VFX shots that I did to improve my skills in many different areas, such as fluid simulation, character animation, camera tracking or the newly released geometry nodes.'
          software: blender
        - yt_id: "ZvaLSyQmPvQ"
          title: "Motion tracking experiments"
          date: 2021-02-13
          description: 'A short project that I did during the winter holidays, along with "Mars drone". I wanted to learn about motion tracking in Blender and did a few different scenes, each one with its own particular challenges.'
          software: blender
        - yt_id: "Mti1cvqTXaE"
          title: "Mars drone"
          date: 2021-02-07
          description: 'Following the hype for the new NASA Mars rover, I decided to do a small scene with its drone flying around the Red Planet.'
          software: blender
        - yt_id: "nhu51PZvFeg"
          title: "Making of Adventure"
          date: 2020-09-11
          description: 'The making of the "Adventure" short film, where I show how the different scenes were constructed and stuff that didn''t make it into the final cut.'
          software: blender
        - yt_id: "uTyFYYW2zEk"
          title: "Adventure"
          date: 2020-09-05
          description: 'One of my biggest projects to date, made during the summer of 2020 (in which I had lots of free time due to the lockdown). It''s a short story about a party balloon that travels the world. My main focus was on the construction of many different landscapes.'
          software: blender
        - yt_id: "Y_VYmBSNjPI"
          title: "Autumn snow"
          date: 2020-02-21
          description: 'My second Blender project, done during the winter holidays. It is a much smaller scene than previous probjects, but I learned about particle and cloth simulation and improved my shading skills.'
          software: blender
        - yt_id: "-48vVeoOj6M"
          title: "Pyke"
          date: 2019-09-23
          description: 'My first serious Blender project. I decided to recreate the castle "Pyke" from "A Song of Ice and Fire" (a.k.a. Game of Thrones). It took me a whole summer, but I learned a lot about Blender along the way!'
          software: blender
        - yt_id: "zDCFct298TY"
          title: "A Song of Ice and Fire animation"
          date: 2016-07-13
          description: 'A very big project made in my free time during my last high school year. It is an animation of the main cities from the ASOIAF (Game of Thrones) universe, in a similar artistic style to the TV serie''s intro. The whole movie was so long in the end that we were unable to render it properly and the final product looks a bit cartoonish.'
          software: maya
        - yt_id: "vZA7HJOHarI"
          title: "House animation"
          date: 2015-06-16
          description: 'Learning Maya in early 2014 I made a 3D model of a house, which I then reused for a high school project. It was the first time I used any sort of ray tracing for rendering.'
          software: maya
        - yt_id: "dWpdAsrJrKA"
          title: "Castle animation"
          date: 2013-11-17
          description: 'My first ever rendered 3D animation, which I did during a weekend after a couple of months of learning Maya.'
          software: maya
  puzzles:
    title: "Puzzle solving"
    description: 'I like puzzles!'
    competitions:
        - title: "Rosalind"
          url: "https://rosalind.info"
          gitlab: "https://gitlab.com/migamic/rosalind"
          tools: [Python]
          description: "I try to focus more on the biology side of the problems to learn more about this field, while solving them in Python as efficiently and concisely as possible. [I have currently solved the first 67 problems](https://rosalind.info/users/migamic/) and I am [ranked #16 in Spain](https://rosalind.info/statistics/countries/es/)."
        - title: "Advent of code 2024"
          url: "https://adventofcode.com/2024"
          gitlab: "https://gitlab.com/migamic/advent-of-code-2024"
          tools: [Python]
          description: 'Third time is the charm. I finally managed to finish all 25 problems on time. I followed the same approach as in 2023, but this time I was more successful.'
        - title: "Advent of code 2023"
          url: "https://adventofcode.com/2023"
          gitlab: "https://gitlab.com/migamic/advent-of-code-2023"
          tools: [Python]
          description: 'My second run of AoC. This time I focused on improving my Python skills (regex, itertools, etc.). I got to day 18.'
        - title: "Advent of code 2022"
          url: "https://adventofcode.com/2022"
          gitlab: "https://gitlab.com/migamic/advent-of-code-2022"
          tools: [C++, Rust, Python, Lua, Perl]
          description: 'My first AoC edition. I decided to try different languages, but I mostly sticked to Rust (to learn it), and falling back to Python or C++ on some days. I got to day 13, but during the following months I continued until day 23.'
---
