---
title: AEGEE-Barcelona website
date: 2024-09-27
github: https://github.com/aegeebarcelona/aegeebarcelona.github.io
tags: [website]
tools: [Astro, React, Tailwind CSS, GitHub pages]
category: fun
---

Official website for the AEGEE-Barcelona student association.

<!--more-->

I made a static website for AEGEE-Barcelona, a student association that I've been involved in for the past months.

![Project image](/images/projects/aegee-about.png)

It was also my excuse to learn Astro, React and Tailwind CSS over the summer holidays.

You can find it currently hosted on [aegeebarcelona.github.io](https://aegeebarcelona.github.io/).
