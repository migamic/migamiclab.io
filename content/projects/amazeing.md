---
title: Amazeing golf
date: 2023-09-30
gitlab: https://gitlab.com/migamic/hacknights
tags: [graphics, 3D]
tools: [Python, threejs]
coauth: [Malika Uteuliyeva]
category: competition
demo: amazeing
toc: true
---

A simple browser game where the user controls a golf ball inside a procedurally-generated labyrinth.

<!--more-->

![Project image](/images/projects/amazeing.png)

## Controls

The user can move forwards and backwards with the `up and down arrow` keys. The camera is rotated with the `left and right arrows`. In order to get a better view of the user's position within the maze, the ball will jump high into the air with `space`.

## Gameplay

The ball is spawned in a random position, and so is a chest. Whenever the user reaches the chest, the game resets into a new random setup.

## Installation and usage

Run the game in a local server with, e.g. `python -m http.server` from the root folder of this repo.

## Hacknights

This project was made by Malika Uteuliyeva & Jaume Ros Alonso during a UPC *hacknight*, a 8h hackathon, from 9pm to 5am.

