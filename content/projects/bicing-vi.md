---
title: Bicing Visualization
date: 2020-12-30
drive: https://drive.google.com/file/d/1rsjSwErlUtqj_XKPjLhPz2pkGhIbVO_e/view?usp=sharing
tags: [visualization, bicing, altair]
tools: [Python, Altair, Pandas, Jupyter Notebook]
coauth: [Álvaro Budría Fernández]
category: class
toc: true
---

A visualization of historical data from Barcelona's bicing system.

<!--more-->

## Part 1

Comparison of a centric and a peripheric station. Using hourly data from the two stations, it shows the availablity of bikes during the day and week.

![Project image](/images/projects/bicing-vi/vi-1.png)

## Part 2

A collection of interactive charts that display aggregated data from all of Barcelona's stations, with added geographical information.

![Project image](/images/projects/bicing-vi/vi-2.png)
