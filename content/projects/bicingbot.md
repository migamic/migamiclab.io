---
title: BicingBot
date: 2019-06-05
drive: https://drive.google.com/file/d/1gk1mGAzaSStFpWzuE7tsuDuSokSqzpHK/view?usp=sharing
tags: [bot, bicing, telegram]
tools: [Python, Telegram, pandas, NetworkX]
coauth: [Álvaro Budría Fernández]
category: class
toc: true
---

A Telegram bot that provides operations with Barcelona’s Bicing system in real time.

<!--more-->

## Start using the bot
Take a look at [Telegram's bot page](https://core.telegram.org/bots) for instructions on how to set up a bot instance. It can be used as any other Telegram bot, in a private chat or a group.  

## Commands
The user interacts with the bot through commands. Here is a list of them with a brief description.  

Any typed command should start with a forward slash (/). Any possible argument a command may require should be separated by a blank space.

### Start
	
	/start

Initializes the conversation with the bot and assigns a default graph to the user with distance 1000.  
It is required before typing other commands.

### Help
	
	/help

Displays a list of possible commands, with a brief description of all of them. May be used before starting.

### Graph
	
	/graph <distance>

Creates a new graph with the given distance (that is, only the nodes that are less than the given distance apart are connected). The new graph will remain the same until a new one is created through the same command.  
Note that by default a graph with distance 1000 will be created. The argument _distance_ is necessary.

### Edges
	
	/edges

Displays the number of edges the current graph has.

### Nodes
	
	/nodes

Displays the number of nodes the current graph has.

### Connected components
	
	/components

Displays the number of connected components the current graph has.  
An isolated station counts as a single connected component.

### Plot graph
	
	/plotgraph

Sends an image of the current graph.

### Route
	
	/route <address #1>, <address #2>

Sends an image of the shortest path between two given addresses of Barcelona.

### Distribute
	
	/distribute <# of bikes> <# of docks>

Displays the cost and most expensive movement to acomplish the minimum cost distribution of bikes that follows the given conditions.  

* _# of bikes_ refers to the minimum number of available bikes any station is required to have.  
* _# of docks_ refers to the minimum number of available docks any station is required to have.  

Updates the number of available bikes and docks for future commands. If the distribution is not possible, nothing is modified.

Both arguments are necessary.

### Update
	
	/update

For efficiency reasons, since the stations are unlikely to change during the conversation, the data is downloaded only once at the beginning and resued to build other graphs with different distances.  

This commmand allows to update the data the graph is built on, downloading it again and constructing a new graph with the same preferences.  

It also updates the current available bikes and docks with real data.

### Authors
	
	/authors

Shows the name of the authors of this project. May be used before starting.

## References
### Official statement of the project
The instructions of the project (in Catalan) can be found [here](https://github.com/jordi-petit/ap2-bicingbot-2019).

### Python modules used
* [NetworkX](https://networkx.github.io/): used for representing and dealing with graphs.  
* [pandas](https://pandas.pydata.org/): used for obtaining and dealing with the dataframes.  
* [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot): used for communicating with the bot.  
* [Haversine](https://pypi.org/project/haversine/): used for obtaining distances through coordinates.  
* [GeoPy](https://geopy.readthedocs.io/en/stable/): used for obtaining the coordinates of addresses.  
* [Static Map](https://github.com/komoot/staticmap): used for generating the images of maps.
