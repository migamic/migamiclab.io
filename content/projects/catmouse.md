---
title: Cat & Mouse (AR game)
date: 2023-01-09
drive: https://drive.google.com/file/d/19NFUauvnrgjxYSq_kgtizbqZ4GocC-YW/view?usp=sharing
tags: [augmented reality]
tools: [Unity, Blender, ARCore]
coauth: [Oriol Rabasseda]
category: class
toc: true
---

A mobile game that uses Augmented Reality.

<!--more-->

Using the most recent advancements in Android AR available in ARCore, we built a game that allows the player to control a mouse running around the floor.

![Project image](/images/projects/catmouse/demo.jpg)

## Gameplay

The player sees the real world through the smartphone's camera. A reticle is placed in the middle of the screen, but always at the height of the floor, and it moves along with the camera. Through this simple input, the player controls the main character, the mouse, which follows the reticle at a high speed.

However, the mouse cannot stay too long in one place, because there is a cat that is permanently chasing it.

There are pieces of cheese scattered around the world, and the goal of the game is to collect as many as possible while avoiding being caught by the tireless cat.

After collecting each piece of cheese, a mouse trap will permanently appear in a nearby position in the world, which the mouse must avoid too, making it harder to move around.


## Augmented Reality

Compared to [my last AR project](/projects/archess) from a few years ago, this one is completely **markerless** and features more advanced techniques, that simply did not exist back then. The most relevant ones are:
* **Plane detection**, to detect the floor or the main planes of the physical scene through the camera. The game takes place on top of the real floor.
* **Ray intersection** between the camera’s direction and the floor. This is used to set the position of the reticle, which the mouse will follow.
* **Light estimation**, to get an approximation of the real illumination and light the virtual scene accordingly to obtain more realistic and immersive rendering.

The game will take a few seconds to start because there needs to be a stage of environment recognition, before objects can be placed. But note that these processes keep working while the game is in motion, which means that the player can keep walking around (e.g. move to other rooms) and the world will keep increasing as long as new stretches of floor are detected.


## Assets

![Project image](/images/projects/catmouse/assets.png)

All the game assets are made in Blender. We went for a low-poly style for simplicity and to ensure a smooth playing experience, even on low-end mobile devices.
