---
title: Guide wars
date: 2023-05-14
github: https://github.com/migamic/hackupc2023
tags: [hackathon, programming, social, visualization]
tools: [React, Python]
coauth: [Malika Uteuliyeva, Evripidis Avraam]
category: competition
toc: true
---

An unfinished multiplayer game that helps tourists learn the layout of a new city.

<!--more-->

The 36-hour hackathon challenge was to build a multiplayer game to entertain plane passengers during their trip. A bonus was to have the game help them learn something about their destination.

![Project image](/images/projects/guidewars/gameplay.png)


## Game description

The player acts as a tourist guide around the streets of a city (their travelling destination), which is viewed top-down as a 2D map. Of course, they want to have the largest tourist crowd around them, since that is seen as a symbol of status in the guide world.

The player moves around the city and picks stray tourists along the way, which makes the surrounding crowd larger. By visiting touristic spots and city landmarks, the chance of finding followers increases, so the player is encouraged to walk around those parts; at the same time, if the player moves around uninteresting city streets, some followers might get bored and leave the group.

At all times, the player is given a destination place through the UI, which he or she needs reach in order to unlock the following one, until the whole tour is completed.

One last thing that players need to take into account is that they are not alone in the city. There are plenty of other tourists guides walking around! If two groups meet with each other, only the largest one will prevail, since people will realize that the corresponding guide is the best one of the two (since he or she had more followers in the first place), so there is no point in sticking to the other one. So beware when getting close to very iconic landmarks, since those are probably going to be bursting with the best guides in the city!


## How it works

The app is built using React. It is playable locally in a web browser, although our goal was to make it available on mobile too, with multiplayer support (through local networks, since Internet access is not guaranteed on an airplane).

A Python script is responsible for generating offline the underlying map background and getting the data for any city in the world. The city name is geocoded and several raster tiles are downloaded from [MapTiler](https://www.maptiler.com/)'s API, which are then stitched together to form a very high resolution image that is used as the background.

The street layout is obtained from [OpenStreetMap](https://www.openstreetmap.org), processed as a graph and exported in a JSON file that is then parsed by the main app program. Likewise, the touristic POIs such as cathedrals, museums or monuments are pulled from OSM.

The main react app displays the image background. On top of it are POIs with their corresponding names, and the player and followers, which are represented with simple colored circles. The camera always moves with the player, whose position is linearly interpolated between graph nodes to ensure a smooth motion.
