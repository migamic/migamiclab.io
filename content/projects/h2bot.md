---
title: H2BOT
date: 2024-06-05
drive: https://drive.google.com/file/d/1LxiQS5z-f-lcoPTyFMCAALhf5nUG3nhp/view?usp=sharing
youtube: https://www.youtube.com/watch?v=2Z6Vr5oOvMQ
tags: [design, sustainability]
tools: [Blender, Photoshop, Canva]
coauth: [Kevinn Giancarlo Castro Farfan, Anita Gabashivili, Evdokia Charalambous, Sofía Larraz]
category: class
toc: true
---

Ideation and design of an autonomous underwater robot for the cleanup of debris in shallow waters.

<!--more-->

![Project image](/images/projects/h2bot/underwater.png)

## Introduction

This was our proposal for the [CBI4AI course](https://phase2.attract-eu.com/projects/cbi4ai/), part of the ATTRACT project from CERN. During 2 intensive months, we were challenged to ideate a solution to help preserve biodiversity and fight pollution in freshwater ecosystems. The presented design needed to be purely theoretical --that is, no need for actually building it in real life, although we were encouraged to construct prototypes to showcase the final idea-- but it had to comply with the laws of physics and be technologically feasible in the near future.

Our team, the _Wild Chickens_, was comprised of 2 engineering students from the Polytechnic University of Catalonia (UPC), Kevinn and me; 2 design students from the European Design Institute (IED), Anita and Evdokia; and 1 business student from the ESADE business school, Sofía.

![Team members](/images/projects/h2bot/team.jpg)

We followed several brainstorming and design methodologies, as well as some intensive research in the problem source, possible relevant technologies and similar existing projects.

## Result

Our final proposal consisted of an underwater robot that would autonomously roam the beds of shallow watter bodies (such as lakes, rivers or sea coasts) in search of solid trash to be collected from the bottom. It would be equipped with a camera and specialized hardware to run computer vision algorithms in real time and detect the target objects; a powerful light would ensure enough visibility. Once a trash object was detected, a balloon would be attached to it, then inflated (the robot is equipped with tanks of compressed air), making it rise to the surface, where it could be easily collected.

We were encouraged to include "futuristic" (i.e. still in early stages of development) technologies from the ATTRACT project into our design. We used [IALL lenses](https://attract-eu.com/attract-stories-behind-the-iall-project/) for our camera, and [UltraRAM](>https://en.wikipedia.org/wiki/UltraRAM) as part of the computational hardware.

![Inside](/images/projects/h2bot/inside.png)

## Presentation

The final result was presented during the gala of the CBI4AI course, as well as displayed all afternoon in our team's booth.

![Presentation](/images/projects/h2bot/presentation.JPG)

We designed 2 posters to introduce the general concept of the solution as well as the technical details. Additionally, we 3D printed a miniature of the robot to show how it would look from the outside.

Finally, we created a short animation in Blender ([YouTube link](https://www.youtube.com/watch?v=2Z6Vr5oOvMQ)) to display in our presentation booth. It explains the motivation for the project, as well as the proposed solution. I mostly did it myself, and it took around a week to produce, from the first storyboard to the final render.

## Future of H2BOT

The presentation of our prototype was met with great enthusiasm from the course organizers, who on top of the quality of the work done, valued our creativity, presentation skills and team dynamics.

In mid-June, a few weeks after the initial presentation for the CBI4AI course, we got offered the possibility to present our project in Grenoble, France, at the [ATTRACT Pre-Final conference](https://phase2.attract-eu.com/attract-pre-final-conference-shaping-the-future-of-innovation-and-collaboration/), where Evdokia went in representation of our team.

In the second half of October, it will be showcased in the [Barcelona Design Week](https://barcelonadesignweek.com/), held in IED.

