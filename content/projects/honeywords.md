---
title: Honeywords
date: 2024-02-23
tags: [webdev, game, top]
tools: [ReactJS, Python, CSS, Vite, bun]
category: fun
demo: honeywords
gitlab: https://gitlab.com/migamic/spelling-bee
---

A web game where players need to construct words from a set of letters.

<!--more-->

My own reimplementation of the popular [NYT's spelling bee game](https://www.nytimes.com/puzzles/spelling-bee), where I keep most of the rules, while adding a few ideas of my own:
* Multiple langugaes: currently it supports English, Spanish, French and Catalan. Both the game words and the UI are translated.
* Add definitions for words.
* A more complex scoring system, where polysemic, palindrome and loop words are also rewarded with extra points.

![Project image](/images/projects/honeywords.png)

My main motivation for this project was to learn React, as well as several aspects of webdev that I had not used before, or had little previous experience, such as:
* Localization with i18next.
* Using local storage to save and load data.
* Responsive websites for mobile.
* Automatic light/dark theme.
* Drawing hexagons with CSS 🙃.

The data comes from [Wiktionary](https://en.wiktionary.org). I chose this data source for several reasons: it is free; it has data for many languages; it contains definitions; it has syntactic information, which allows me to filter out words like plurals, verb forms, etc; and it can be downloaded and parsed offline, so no internet connection is required after the game has loaded initially (especially convenient when commuting on the train and losing connection every 5 seconds;). However, I did not anticipate how hard the data would be to parse; requiring slightly different parsers for each language and more regexs than all I've used in the previous decade combined.

The project took much longer to finish than I originally anticipated, but I managed to implement all the features that I planned initially :)
