---
title: Jeopardy
date: 2025-01-01
tags: [webdev, game]
tools: [ReactJS, Tailwind CSS, Vite, bun]
category: fun
demo: jeopardy
gitlab: https://gitlab.com/migamic/jeopardy
toc: true
---

A web game where players unlock questions of their chosen difficulty, and get points if they guess it correctly.

<!--more-->

The game is an online version of [_Jeopardy!_](https://en.wikipedia.org/wiki/Jeopardy!). All online alternatives that I know of are very limited or require a subscription, so I made my own.

## How does the game work?

Players are split into teams, or play individually one player per team. At each turn, the current team will choose a question from the set; they are split into categories, and labeled with their difficulty, but the question is initially hidden.

![Initial layout](/images/projects/jeopardy/initial.png)

After choosing, the question is displayed and the team guess an answer. If it is correct, the answer is displayed and the question is solved; if not, the next team can try to guess it, and so on until a full round is made, at which point the answer is revealed, and no one gets points.

![Question](/images/projects/jeopardy/question.png)

The team who solved the question will get the points that the question was worth. Then it will be the turn of the next team to try their luck. The game goes on until all questions are revealed; the team that gets the most points in the end, wins.

![Mid game](/images/projects/jeopardy/mid_game.png)


## Features

Here is a list of some features I implemented. I made it very ad-hoc for my specific use case. I might extend it in the future if I need to use it again.

* Unlimited number of questions, split into categories.
* Custom amount of points for each question.
* Option to accompany the questions and/or answer accompanied by an image.
* Answered questions remain visible.
* Unlimited number of teams, with configurable names and colors.
* Manually modify the points of a team during the game, if needed (by clicking on the team score).

![Question image](/images/projects/jeopardy/question_img.png)

## How to run it 

You can [test it out from your browser](/demos/jeopardy). I have set it up with 5 teams and a set of basic AI-generated questions.

If you want to use your own setup, you need to get [the source code](https://gitlab.com/migamic/jeopardy) and build it yourself. In the future, I might implement the option to add the data from the browser, so this is not needed.

Modify the existing JSON template with your own data, and you are good to go!
