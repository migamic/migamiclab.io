---
title: Magellan
date: 2024-05-05
gitlab: https://gitlab.com/migamic/hackupc2024
tags: [hackathon, programming, social, traveling, app]
tools: [Vue, CSS, Python]
coauth: [Malika Uteuliyeva, Lucas Aguiño, Laura Pérez]
category: competition
toc: true
---

A sketch of a social media app for travelers that gamifies the experience of visiting a city by proposing collaborative challenges.

<!--more-->

The 36-hour hackathon challenge was to build a product to increase the social aspect of visiting places for solo travelers.

We sketched a social media app that proposes challenges to travelers in the form of places to visit in a city. Challenges are completed by visiting that place and taking a picture of it. To enhance the visiting experience, the app offers descriptions and interesting facts of every place; it also proposes collaborative challenges, that require users to visit a place together.

The amount of challenges is adjusted to the user's available time when visiting a place. The app also computes matches between users that coincide in the same city to create collaborative challenges accordingly.

## App description

### Home screen

The user is greeted with a home screen that shows recent trips from friends. These are shown as a collection of photos taken during the trip (the challenges the friends completed during their trips).

![Home screen](/images/projects/magellan/home_screen.png)

The UI is fully interactive, so the user can expand on a particular trip and see more details.

### New trip

The main feature of the app is the creation of challenges for an upcoming trip. The '+' button on the bottom allows the user to do so.

![Trip creation screen](/images/projects/magellan/new_trip.png)

The trip creation screen prompts the user to select a destination city (currently we only consider main cities, since the challenges are created based on well-known landmarks), dates for visiting and the reason for visiting.

The number of days spent in the city and the type of visit (e.g. tourist or business) will serve to estimate the available time of the user, and the amount of challenges will be set accordingly. The current system has a score of how _interesting_ places are, as well as how long it takes to visit them; then computes the optimal list of challneges so that the user can get the most out of the available time.

The specific dates are also used to find matches with other users of the app visiting the same city.

### Challenge list

Once the user creates a trip, a series of challenges is presented. These consist of a list of places, mostly famous and interesting landmarks, to visit.

![Challenge list screen](/images/projects/magellan/challenge_list.png)

The goal of the user is to visit as many of those as possible during his or her stay in the city.

Selecting a place will open the place description page, where extra details are provided, such as the description and interesting facts, and instructions on how to get there. Additionally, the app will show the place as not visited until the user visits it, takes a photo and uploads it, completing the challenge.

![Place info screen](/images/projects/magellan/place_info.png)

### Profile page

Lastly, there is a profile page where the user can see all of his or her trips --past, current and upcoming--, along with some statistics of the total number of places visited.

![Profile screen](/images/projects/magellan/profile.png)

Again, any trip can be expanded to see more details, like all the gallery of pictures taken there. Individual places/challenges can also be selected.

![Completed place](/images/projects/magellan/completed_place.png)

## Future work

The current sketch is a web app for browsers targeting a mobile experience. A final product should have a native mobile app, possibly complemented with a proper web app for desktop users.

Again, our current product is a very unpolished version of our vision for the final app. We focused on building a frontend to showcase the main features and workflow of the app, but it is very far away from a working state. While we built some of the main backend components (automated description of places, challenge selection based on available time and interest of landmarks, overlaps between users to find matches, etc.), it is not yet properly connected to the frontend.

We also think that the final product should include more features essential in a social media app, such as adding friends and some sort of message/reaction system, custom recommendations on the feed page, and the possibility to easily share photos and achievements to other social media platforms.

It would also be convenient to include a reward system to encourage local users to meet with travelers visiting a city and help them complete challenges while offering local knowledge.

Finally, an interesting feature for the app would be to include some image recognition system to automatically verify that a user has indeed visited a place and taken a photo of it. While the social aspect of it (the fact that images are shared among the user's friends) disencourages faking having visited a place, we think that a system to verify it and mark it as completed would be welcomed by users.
