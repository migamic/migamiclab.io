---
title: Rare diseases in Catalonia
date: 2019-12-15
gitlab: https://gitlab.com/migamic/hack-marato
tags: [hackathon, programming, social, visualization]
tools: [HTML5, CSS3, Leaflet, C++]
coauth: [Miquel Martínez de Morentin, Anxo Lois Pereira Canovas, Emili Bonet Cervera ]
category: competition
---

A sample website that shows an interactive map of the location and information of ficitional individuals with rare diseases across Catalonia.

<!--more-->

![Project image](/images/projects/mapmarato.png)

The motivation was to raise awareness of the huge number of rare disease cases, which was the topic of this year's [La Marató](https://www.ccma.cat/tv3/marato/en/2019/230/) and [UPC's associated hackathon](https://www.fib.upc.edu/ca/la-marato).

It also has an HTML form that allows new user data to be introduced. The information is parsed from a JSON file where all the records are stored.
