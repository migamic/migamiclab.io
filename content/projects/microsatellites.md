---
title: Rabbit microsatellite analysis
date: 2020-05-14
drive: https://drive.google.com/file/d/1QnaBq6zYAKRpkePavPx2GUw09JGAT0gY/view?usp=sharing
tags: [data analysis, biology, genetics]
tools: [R, RStudio]
coauth: [Álvaro Budría Fernández]
category: class
---

Analysis of a database of genetic data from different species of rabbits.

<!--more-->

The data set consisted of 45 allele attributes (two values each) from ~500 individuals. One of the main challenges turned out to be the preprocessing of the data, such as dealing with missing values or inaccurate measurements, which represented a large part of the data set.

![Project image](/images/projects/microsatellites.png)

From the many data analysis techniques tried, the most successful were Principal Component Analysis and Multidimensional Scaling for distinguishing wild rabbits from domestic ones. This shows that there is a significant genetic difference between these two groups, as opposed to the multiple species within each, that prove much harder to identify.
