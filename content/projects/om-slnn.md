---
title: SLNN for digit recognition
date: 2020-03-15
drive: https://drive.google.com/file/d/1D7WWD2ct7RMk2pj63Cqccnnbia61TbjZ/view?usp=sharing
tags: [optimization, neural network, digit recognition]
tools: [Matlab]
coauth: [Emili Bonet Cervera]
category: class
---

A Single Layer Neural Network coded from scratch that performs digit recognition in images with more than 95% accuracy.

<!--more-->

The goal of the project was to compare different hand-coded optimization algorithms in terms of accuracy, speed of convergence and computational cost. The ones tried were _Gradient Method_ (GM), _Stochastic Gradient Method_ (SGM) and the _Broyden–Fletcher–Goldfarb–Shanno algorithm_ (BFGS).

The data used was a subset of 500 images from the [MNIST database](http://yann.lecun.com/exdb/mnist/), split in half for training and testing. The pixels were flattened before being inputted to the network, which was trained to output a binary answer on whether the image contained X digit or not.

![Project image](/images/projects/om-slnn.png)

The digit 3 was the hardest to identify, where the model obtained only 86% accuracy, probably due to its resemblance to the shape of the 8. In the image above, pink and red images are false positives and negatives, respectively; while green and blue ones are correct answers.
