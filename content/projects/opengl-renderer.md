---
title: Optimized OpenGL renderer
date: 2023-06-02
drive: https://drive.google.com/file/d/1PhN68lOHn0QKA1ScRTxNAa44PoR08EfC/view?usp=sharing
tags: [opengl]
tools: [OpenGL]
category: class
---

A 3D renderer built with OpenGL for testing different performance optimization strategies.

<!--more-->

The program renders a grid of NxNxN 3D objects and the camera moves around the scene in a controlled manner, showing at times all the objects, only a fraction or none. The goal of the project was to try different real-time rendering optimizations and mesure the effect on the frame rate.

The following optimization strategies are implemented and can be toggled on/off in the settings:
* **Frustum culling**: compute the view frustum of the camera at every frame. Objects that completely fall outside the view frustum do not need to be rendered. Since computing this for every single triangle can be very expensive (the number of triangles can be huge when the scene has multiple high-poly meshes), a conservative approximation with bounding spheres is used.
* **Occlusion queries**: compute, for every entity whether it is visible or not, even if it is in the view frustum. Entities that are occluded by other ones do not need to be rendered. Using the result of the occlusion query from the previous frame causes some small visible artifacts, but has much better performance, since the CPU and GPU can work more asyncronously. As before, bounding boxes are used to greatly speed up the process at the cost of some accuracy.
* **Lazy updates**: improve the speed of occlusion queries by assuming that a visible object will remain visible for at least the N following frames, and only repeat the query after that.

Additionally, the user can choose between different meshes of different complexity and phong/goraud shading.

A detailed report of the results with additional analysis can be found in the attached files.
