---
title: Planet explorer (VR game)
date: 2022-11-05
drive: https://drive.google.com/file/d/1DMm68WO89UB1ZesKcJTODK4J0-M3faEP/view?usp=sharing
tags: [virtual reality]
tools: [Unity, Blender]
coauth: [Oriol Rabasseda]
category: class
toc: true
---

A Virtual Reality mobile game.

<!--more-->

The player controls a robot, similar to a Mars rover, that explores foreign planets. 
The game is played on a mobile device, that can be attached to a headset such as Google Cardboard. The player sees the world in first-person through the robot's camera; the camera rotation is synced to the head rotation.
The movement of the robot and the different actions are controled by a separate joystick controlled that is connected by Bluetooth to the Android device.

![Project image](/images/projects/planetexplorer/img1.jpg)
![Project image](/images/projects/planetexplorer/img2.jpg)


## Gameplay

The rover, through whose camera the player is looking, is placed on the surface of a fictional desertic planet, with several objects scattered around it. It can move in any direction along the surface while being within a limited radius of the origin – a constraint we added in order to avoid implementing dynamic world generation, which was beyond the scope of the project.

When the robot is within arm’s reach of a selectable object (all objects except the terrain are selectable), it will light up when it is pointed to (i.e. in the center of the player’s view field). A lit-up object indicates that it can be grabbed by the robot; if the player chooses to do so, an extensible arm appears and the object is raised from the ground and brought in front of the player’s view.

While selected, an object will always be in front of the player, even if the camera is moved. It can also be rotated to allow its inspection from multiple angles. Text with information about the object is displayed on top of it, in an AR-fashion. At any point, a selected object can be dropped to the original position and the player is allowed to move again.

## World generation

The world is procedurally generated. The terrain height is determined by a random Perlin noise texture, with parameters such as the mountain (or dune, rather) width and height following a uniform random distribution between two bounds manually set.

The material of the terrain and skybox are also random, chosen from a set of 4 predefined options (allowing for 16 different combinations in total).

Finally, the assets are generated randomly within a radius from the origin, the models being chosen from a premade set and its size and rotation being randomized per instance.

## Assets

![Project image](/images/projects/planetexplorer/robot.jpg)

All assets, except for the skyboxes, were modeled by us in Blender. We followed a low-poly style for simplicity, aesthetics and performance reasons.
