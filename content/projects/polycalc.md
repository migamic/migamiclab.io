---
title: Polygon calculator
date: 2019-03-31
drive: https://drive.google.com/file/d/13gXer5_-u4Lejr5juVL-BdgGeD23KWTA/view?usp=sharing
tags: [polygon, calculator]
tools: [C++]
category: class
toc: true
---

A polygon calculator, used to perform operations between convex polygons and output the result, in text or image format. 

<!--more-->

It has support for two point, single point and empty polygons as well. Includes examples of normal use and some extreme cases.

## Installation:

To run the program it is necessary to have the library "pngwriter", that allows for the generation of image files. If it is not yet installed execute the following commands in order to do so:

    git clone https://github.com/pngwriter/pngwriter.git
    cd pngwriter
    cmake -DPNGwriter_USE_FREETYPE=OFF -DCMAKE_INSTALL_PREFIX=$HOME/libs .
    make
    make install
    cd ..
    rm -rf pngwriter

>The library should now be installed under /home/libs. If it is in another directory, the "Makefile" of this program should be modified to indicate the actual one in order to compile the project.

Decompress the package file and open a new console in the new directory. Compile the project using the command `make`. Then execute the program using `./calculadora.exe`.
The polygon calculator is now ready to accept new instructions. There is a sample file with all the commands along with the other files. To run those instructions, execute `./calculadora.exe < sample.txt`.

## Introducing data:

Operations are introduced through the standard input. That is, the user can either write it on the terminal emulator during runtime or let the program read from a text file using the "<" operator when executing.
Every command, along with all its atributes must be written in the same line, otherwise they will be considered to be multiple commands. Every command must also be in a different line.

## Syntax:

    <command> <file> <new name> <existing name/s> <other possible data>

Every parameter must be separated by, at least, one blank space. What attributes are necessary, along with possible alternatives and specific syntax, is detailed under every command.

## Output:

The printed output will be written through the standard output, this is, by default will be written in the terminal emulator, but it can easily be redirected to a file or a pipe. Every number printed will have four decimal digits.
Any operation that creates a file of any type will do so in the current directory, that is, the folder where the program was executed.

## Possible operations and commands:

### Create a new polygon

    polygon <name> <point1X> <point1Y> <point2X> *etc*

It must be followed by its name and a set of points, given first by the x coordinate and followed by the y coordinate. If more points are given than necessary, the program will simplify it to a convex polygon by keeping only those that belong to the convex hull.
If no points are given, the polygon is assumed to be empty. Repeated points are only counted once.
If a polygon with the same name already exists it will be overwritten, and the new one will not keep any of the properties of the other. If no name is given, the command will be ignored.

### Comment

    # <anything>

Any command starting with the hash sign (#) will be ignored by the program and it will output a hash. Everything that follows on that same line will also be ignored.

### Print a polygon

    print <name>

The program will output the points of an existing polygon. The order of the points may have changed from those first introduced and redundant points will have been deleted.

### Area of a polygon

    area <name>

Returns the area of an existing polygon. Single points and segments have area 0.

### Perimeter of a polygon

    perimeter <name>

Returns the length of the perimeter of an existing polygon. Single points have no perimeter.

### Number of vertices

    vertices <name>

Returns the number of vertices of an existing polygon. If the polygon introduced had redundant points, it will output the number of vertices of its convex hull.

### Check if is regular

    regular <name>

Says whether the given existing polygon is regular or not. The given polygon must have at least 3 vertices.

### Centroid

    centroid <name>

Returns the x and y coordinates of the centroid of an existing polygon.

### List of all polygons

    list

Outputs the name of all the polygons introduced, lexycographically sorted.

### Save to a file

    save <filename> <name1> <name2> *etc*

Saves all the specified existing polygons in a text file, to be loaded again at any time. Color will not be saved.

### Load a save file

    load <filename>

Loads all the polygons from a "save" file as if they were introduced for the first time.

### Change the color

    setcol <name> <red value> <green value> <blue value>

Modifies the color of a given polygon. By default its color is black. The input must be in three numbers ranging from 0 to 1 associated with the rgb representation of that color (that is, the red, the green and the blue components of the color respectively).

### Draw polygons

    draw <filename>.png <name1> <name2> *etc*

Draws any number of already existing polygons to the specified file. They may be moved and scaled to best fit in the image, but their proportions and relation to one another will not change. Empty polygons and single points will not be drawn.

It will output an image with the given name with resolution 500 x 500 and white background. White polygons will not be visible.

It is not strictly necessary to add any extension to the output file, but it is strongly suggested to add ".png" at the back of the name to avoid problems when opening it afterwards.

### Intersection

    intersection <name1> <name2>
    intersection <name> <name1> <name2>

Computes the intersection of two existing polygons. The input can be given in two ways after the "intersection" command:
* Two names of already existing polygons: will substitute the first one to be the intersection of the two.
* Three names (the two last ones of existing polygons): a polygon with the first name will be created being the intersection of the two (if a polygon with the first name already existed it will be substituted).

Any intersection with an empty polygon will result in an empty polygon.

### Convex union

    union <name1> <name2>
    union <name> <name1> <name2>

Computes the convex union of two existing polygons. The input can be given in two ways after the "union" command:
* Two names of already existing polygons: will substitute the first one to be the convex union of the two.
* Three names (the two last ones of existing polygons): a polygon with the first name will be created being the convex union of the two (if a polygon with the first name already existed it will be substituted).

If the union is not a convex polygon, its convex hull will be computed.

### Check if one is inside another

    inside <name1> <name2>

Says whether the first polygon is inside the other or not. Points in common will be considered inside; for instance, a polygon is inside of itself.

### Compute the bounding box

    bbox <name> <name1> <name2> *etc*

Creates the bounding box of a given set of already existing polygons, that is, the smallest rectangular polygon that includes all. In most cases it will have four vertices, but it may also have two (the bounding box of a vertical or horizontal line), one (the bounding box of a single point) or be an empty polygon (the bounding box of an empty polygon).
If no polygons are specified, the bounding box will be an empty polygon.

