---
title: Parallel HPC experiments
date: 2020-04-14
drive: https://drive.google.com/file/d/1vbtMbmKEtjNSDsBkyVlO7S6Snk5N-lgb/view?usp=sharing
tags: [parallel computing, remote, hpc]
tools: [Slurm, Bash]
coauth: [Álvaro Budría Fernández]
category: class
---

Comparison of different parallelism techniques in a High Performance Computing environment (BSC's MareNostrum supercomputer).

<!--more-->

We measured the execution time of some programs from the [NAS Parallel Benchmarks](https://www.nas.nasa.gov/software/npb.html), which are designed especially to evaluate the performance of parallel supercomputers, using OpenMP (for shared-memory multiprocessing), MPI (for distributed-memory setups) or a combination of both.

Our analysis also included insights into the percentage of actual CPU time vs communication between nodes, energy consumption and how these metrics changed as the number of computation nodes was increased (we tested with up to 384 CPUs).
