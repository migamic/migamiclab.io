---
title: Sneaky reader
date: 2024-02-23
github: https://github.com/migamic/sneaky-reader
tags: [graphics, 3D, computer vision, tracking, top]
tools: [React, JavaScript, HTML, CSS]
coauth: [Lucas Aguiño, Malika Uteuliyeva, Adilet Tuleuov]
category: competition
---

An ebook reader masked as an IDE. So people can read novels at work without looking suspicious.

<!--more-->

This project was done during an 7-hour hackathon (9:30 p.m. to 4:30 a.m.) and is unfinished.

The idea is to have a lightweight webapp that parses ebook text and displays it as if it were code, with line breaks, syntax highlighting, etc.

The features currently implemented are:
* Basic IDE interface, with fake buttons, tabs, etc.
* Basic ebook parsing, adding syntax highlighting and code keywords.
* Split the book into chapters, each displayed as a different file in the sidebar.
* The book text appears as the user types.

Some of the remaining features are:
* Improved ebook support. Not all ebooks are parseable now.
* Improved syntax and code formatting. Add more colors and overall syling improvements.
* More complex (fake) UI, with more buttons and others.
* User settings to enable/disable features such as text appearing as you type.
