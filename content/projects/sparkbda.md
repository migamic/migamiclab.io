---
title: Predictive analysis on aircraft data
date: 2021-01-05
drive: https://drive.google.com/file/d/1qDQaTlsPZjluB1dNEuq4_TTtCYGGoMZi/view?usp=sharing
tags: [spark, data analysis, prediction]
tools: [Spark, Hadoop, PostgreSQL, Python, MLlib]
coauth: [Anxo-Lois Pereira]
category: class
---

Classification of aircraft maintenance status with a distributed system.

<!--more-->

A distributed decision tree classifier built with Spark that predicts, given KPIs of a particular aircraft, whether it will need to be in maintenance soon or not. The whole process is split into three separate pipelines.

**Data management pipeline**

![Project image](/images/projects/sparkbda/data_management.png)

The system reads data from different sources, including csv text files, remote databases and our own data warehouse using a Hadoop Distributed File System. The data is then filtered and aggregated using Spark transformations and outputted as a single text file with a dataframe-like structure with only the relevant attributes.

**Data management pipeline**

![Project image](/images/projects/sparkbda/data_analysis.png)

A random split is then performed to obtain train and test partitions and the classifier from MLlib is trained on the former. The trained model is saved for further use and it is also tested with the remaining data to obtain an estimate of its accuracy.


**Run-time Classifier pipeline**

![Project image](/images/projects/sparkbda/run_time.png)

When new data arrives, this pipeline performs a similar preprocessing to the data management one and loads the previously trained classifier. The result is outputted to the user.
