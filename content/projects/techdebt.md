---
title: Dealing with technical debt
date: 2021-10-31
gitlab: https://gitlab.com/TAED-tech-debt/Dealing-with-Tech-Debt
tags: [hackathon, data analysis]
tools: [Python, Jupyter notebook, SQLite, LanguageTool]
coauth: [Anxo-Lois Pereira Cánovas, Verónica Rina Garcia, Álvaro Budría Fernández]
category: class
---


Data Analysis project to explore the relationship between good software engineering practices and the quality of the produced code.

<!--more-->

We developed a set of tools to analyze coding practices in the [Technical Debt Dataset](https://github.com/clowee/The-Technical-Debt-Dataset), with data providing from SonarQube reports on multiple open-source projects of different scale.

We used multiple variables from the data such as the number of commits, the severity of bugs created or the grammatical correctness of commit messages.

It was developed strictly following the CRISP-DM methodology.


The complete project report can be found [here](https://gitlab.com/TAED-tech-debt/Dealing-with-Tech-Debt/-/blob/main/reports/Curmudgeons_Report.pdf).
