---
title: Prediction of energy production
date: 2020-06-04
drive: https://drive.google.com/file/d/1JyG1KAEzyU-vAbKccmbwAoQKtIB0je1f/view?usp=sharing
tags: [time series, prediction, energy]
tools: [R, RStudio]
coauth: [Álvaro Budría Fernández]
category: class
---

Time series analysis and prediction of the production of renewable energy in the United States.

<!--more-->

![Project image](/images/projects/timeseries.png)

The dataset used corresponds to monthly measurements of renewable energy production in the US from 1990 to 2018, both included.

We used R's powerful built-in time series analysis tools to identify trends, yearly patterns, outlier values and others. We also built ARIMA models to forecast future values not yet present in the data.
