---
title: Virtual museum
date: 2023-06-12
drive: https://drive.google.com/file/d/1MBAp41B0tEaAB-6UcNZga-LeNRsmvtnj/view?usp=sharing
tags: [opengl]
tools: [OpenGL]
category: class
---

An optimized OpenGL renderer of 3D models.

<!--more-->

The user is able to move around a simple 3D labyrinthine building with complex 3D objects. The goal of the project is to test different graphical optimizations in order to offer the user a smooth experience while rendering a very complex scene.

![Project image](/images/projects/virtualmuseum/lods.png)


The main features of the program are:
* **Layout loading**: the layout of the museum (rooms, corridors and positioning of the objects) is specified in a text file. Multiple layouts of very different magnitude have been tried to test that the program can handle large scenes with ease.
* **Simplification**: a vertex clusering simplification (with either the average cluster position or Quadric Error Metrics) is applied to 3D meshes in a preprocessing stage. Additionally, normal clustering can be set, to enable the preservation of thin mesh features. The results are saved to disk to avoid recomputing them every time.
* **Time-critical rendering**: at startup, the program estimates the computer's graphical performance. During runtime, the models' level of detail is set dynamically in order to ensure the maximum possible quality while still maintaining a smooth frame rate. A hysteresis transition takes place to avoid a model's LoD constantly changing.
* **Visibility testing**: a separate program is used to precompute cell-to-cell visibility of a given layout. Tracing 1 million rays with random directions from every cell, the visible cells are computed and stored in a separate file. The main program then uses this information to determine which objects are not visible at all from the user's position and don't need to be rendered.

![Project image](/images/projects/virtualmuseum/simplification.png)
