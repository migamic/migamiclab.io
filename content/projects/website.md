---
title: Personal website
date: 2022-07-10
gitlab: https://gitlab.com/migamic/migamic.gitlab.io
tags: [website]
tools: [Jekyll, Liquid, HTML5, CSS3 / SCSS, GitLab]
category: fun
---

This website!

<!--more-->

It is build with Jekyll (a Static Site Generator) and deployed using GitLab pages.

I started with the [Prologue template](https://html5up.net/prologue) but had to do some HTML and CSS trickery to get things looking the way I wanted. Other than that, Jekyll makes it very easy to add new content once the whole system is working, so hopefully I will remember to keep it updated!

It was also my first time experimenting with GitLab's continuous integration tools, and probably not my last.
