---
title: Personal website (V2)
date: 2024-03-31
gitlab: https://gitlab.com/migamic/migamic.gitlab.io
tags: [website]
tools: [Hugo, GitLab]
category: fun
---

A new version of this website.

<!--more-->

A year and a half after building the [first version of my website](/projects/website), I decided to make a new one from scratch. This time I'm using Hugo instead of Jekyll. 

This new version features several improvements over the previous one: light/dark theme, more responsiveness and faster loading times, and a refreshed look-and-feel.
