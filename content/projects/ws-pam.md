---
title: Remote image database
date: 2022-01-12
drive: https://drive.google.com/file/d/1dd33OjcFDXSGx9HJFjJ-0yj_0tU74UJ8/view?usp=sharing
tags: [web service, soap, rest, images, database]
tools: [Java, JSP, HTML5, CSS3, Netbeans, Java DB]
coauth: [Anxo-Lois Pereira]
category: class
---

A dynamic webpage with a SOAP and REST web service used for uploading, searching and downloading images.

<!--more-->

![Project image](/images/projects/ws-pam/menu.png)

The client's web-based application allows registering (_i.e._ uploading) an image, listing all the images currently stored on the server and searching through them. The backend runs independently, meaning that different client-side applications could be run using the same web service.

![Project image](/images/projects/ws-pam/register.png)

When an image is registered, the client is able to add some metadata to it, that will be stored and used when searching. The service also includes the possibility of performing some server-side encryption, which would protect sensitive data from potential data breaches on the database; the image is encrypted before storing it and decrypted before being returned to the user.

XML encryption is also performed in the SOAP web service to ensure a more secure communication. Only the sensitive parts of the XML payload are encrypted in order to reduce the computational cost.

![Project image](/images/projects/ws-pam/search.png)

The service offers search capabilities, where the user can enter some data in the search fields that will be translated into an SQL query, giving as a result the images that fulfill such conditions.

The system is able to perform _fuzzy search_, meaning that the input need not match perfectly the image metadata in order for it to be shown; a partial match of the string will also find that image.

![Project image](/images/projects/ws-pam/list.png)

The image results, whether from the _search_ or _list_ functions, are listed in a table displaying all the metadata, plus a clickable miniature of the image to download the original. Note that in this case we show all the raw metadata as it is stored in the database; in a real-life scenario, the client application would probably hide some of this data, or display it in a more user-friendly manner.
