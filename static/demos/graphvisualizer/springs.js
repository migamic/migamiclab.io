
var canvas = document.getElementById("myCanvas");
var c = canvas.getContext("2d");

canvas.width = window.innerWidth - 480;
canvas.height = window.innerHeight - 20;

const node_color = getComputedStyle(document.documentElement).getPropertyValue('--node-color');
const edge_color = getComputedStyle(document.documentElement).getPropertyValue('--edge-color');
const selected_color = getComputedStyle(document.documentElement).getPropertyValue('--selected-color');

var simMinWidth = 20.0;
var cScale = Math.min(canvas.width, canvas.height) / simMinWidth;
var simWidth = canvas.width / cScale;
var simHeight = canvas.height / cScale;

var particles;
let selectedParticle = -1;
var sim_running = true;


document.getElementById('play-pause').addEventListener('click', function() {
  sim_running = !sim_running;
  if (sim_running) {
    update();
  }
});

document.getElementById('reset').addEventListener('click', function() {
  init_particles();
  draw();
});


function cX(pos) {
  return pos.x * cScale;
}

function cY(pos) {
  return canvas.height - pos.y * cScale;
}

function init_particles() {
  const numParticles = document.getElementById("num-particles").value;
  particles = [];
  for (var i = 0; i < numParticles; i++) {
    var particle = {
      id: i,
      radius: 0.2,
      pos: {x: Math.random()*simWidth, y: Math.random()*simHeight},
      vel: {x: 0.0, y: 0.0},
      acc: {x: 0.0, y: 0.0},
      neighbors : []
    };
    particles.push(particle);
  }

  // Set neighbors
  const P = document.getElementById("edge-prob").value;
  for (var i = 0; i < numParticles; i++) {
    var p1 = particles[i];

    for (var j = i+1; j < numParticles; j++) {
      var p2 = particles[j];
      if(Math.random() < P) {
        p1.neighbors.push(p2.id);
        p2.neighbors.push(p1.id);
      }
    }
  }
}

function draw() {
  c.clearRect(0, 0, canvas.width, canvas.height);

  // Draw edges
  c.strokeStyle = edge_color;
  for (var i = 0; i < particles.length; i++) {
    const p = particles[i];
    for (var j = 0; j < p.neighbors.length; j++) {
      const nid = p.neighbors[j];
      if (nid > p.id) {
        const neighbor = particles[nid];
        c.beginPath();
        c.moveTo(cX(p.pos), cY(p.pos));
        c.lineTo(cX(neighbor.pos), cY(neighbor.pos));
        c.stroke();
      }
    }
  }

  // Draw nodes
  for (var i = 0; i < particles.length; i++) {
    if (i == selectedParticle) {
      c.fillStyle = selected_color;
    } else {
      c.fillStyle = node_color;
    }
    const p = particles[i];
    c.beginPath();			
    c.arc(
      cX(p.pos), cY(p.pos), cScale * p.radius, 0.0, 2.0 * Math.PI); 
    c.closePath();
    c.fill();			
  }
}

// Mouse interaction
canvas.addEventListener('mousedown', function(e) {
  const rect = canvas.getBoundingClientRect();
  const x = e.clientX - rect.left;
  const y = e.clientY - rect.top;

  // Check if a particle is clicked
  for (let i = 0; i < particles.length; i++) {
    const p = particles[i];
    const distance = Math.hypot(cX(p.pos) - x, cY(p.pos) - y);
    if (distance - cScale * p.radius < 0) {
      selectedParticle = i;
      break;
    }
  }
});

canvas.addEventListener('mousemove', function(e) {
  if (selectedParticle >= 0) {
    const x = e.clientX;
    const y = canvas.height - e.clientY;
    particles[selectedParticle].pos = {x: x / cScale, y: y / cScale};
    draw();
  }
});

canvas.addEventListener('mouseup', function(e) {
  selectedParticle = -1;
});

function coulombForce(p1, p2) {
  const k = document.getElementById("repulsion").value;
  const q1 = 10;
  const q2 = 10;

  const dx = p2.pos.x - p1.pos.x;
  const dy = p2.pos.y - p1.pos.y;
  const distance = Math.sqrt(dx*dx + dy*dy);

  if (distance < 0.0001) {
    return {x: 0.0, y: 0.0};
  }

  let forceMagnitude = k * Math.abs(q1 * q2) / (distance * distance);
  forceMagnitude = Math.min(forceMagnitude, 100.0); // Clip force magnitude to 10
  forceMagnitude *= -1; // Repulsive force

  const forceDirection = {x: dx / distance, y: dy / distance};
  const force = {x: forceMagnitude * forceDirection.x, y: forceMagnitude * forceDirection.y};
  return force;
}


function springForce(p1, p2) {

  if (!p1.neighbors.includes(p2.id)) { // Symmetry assumption
    return {x: 0.0, y: 0.0};
  }

  const dx = p2.pos.x - p1.pos.x;
  const dy = p2.pos.y - p1.pos.y;
  const distance = Math.sqrt(dx*dx + dy*dy);

  if (distance < 0.0001) {
    return {x: dx, y: 0.0};
  }

  const forceDirection = {x: dx / distance, y: dy / distance};
  const k = document.getElementById("spring").value;

  const force = {x: k * forceDirection.x, y: k * forceDirection.y};
  return force;
}


function dragForce(p) {
  const k = document.getElementById("drag").value;

  const force = {x: -k * p.vel.x, y: -k * p.vel.y};
  return force;
}

function simulate() {
  for (var i = 0; i < particles.length; i++) {
    const p = particles[i];
    if (isNaN(p.pos.x)) {
      return;
    }
  }

  // Reset accelerations
  for (var i = 0; i < particles.length; i++) {
    var p = particles[i];
    p.acc.x = 0.0;
    p.acc.y = 0.0;
  }

  // Compute forces
  for (var i = 0; i < particles.length; i++) {
    var p1 = particles[i];
    for (var j = i+1; j < particles.length; j++) {
      var p2 = particles[j];
      const repulsive_force = coulombForce(p1,p2);
      p1.acc.x += repulsive_force.x;
      p1.acc.y += repulsive_force.y;
      p2.acc.x -= repulsive_force.x;
      p2.acc.y -= repulsive_force.y;

      const attractive_force = springForce(p1,p2);
      p1.acc.x += attractive_force.x;
      p1.acc.y += attractive_force.y;
      p2.acc.x -= attractive_force.x;
      p2.acc.y -= attractive_force.y;
    }

    const drag_force = dragForce(p1);
    p1.acc.x += drag_force.x;
    p1.acc.y += drag_force.y;
  }



  // Integrate velocities and positions
  for (var i = 0; i < particles.length; i++) {
    if (i != selectedParticle) {
      const timeStep = document.getElementById("time-step").value;
      var p = particles[i];
      p.vel.x += p.acc.x * timeStep;
      p.vel.y += p.acc.y * timeStep;
      p.pos.x += p.vel.x * timeStep;
      p.pos.y += p.vel.y * timeStep;
    }
  }


  // Boundary collision
  const ep = 0.1;

  for (var i = 0; i < particles.length; i++) {
    var p = particles[i];

    if (p.pos.x < 0.0) {
      p.pos.x = 0.0;
      p.vel.x = -p.vel.x*(1-ep);
    }
    if (p.pos.x > simWidth) {
      p.pos.x = simWidth;
      p.vel.x = -p.vel.x*(1-ep);
    }
    if (p.pos.y < 0.0) {
      p.pos.y = 0.0;
      p.vel.y = -p.vel.y*(1-ep);
    }
    if (p.pos.y > simHeight) {
      p.pos.y = simHeight;
      p.vel.y = -p.vel.y*(1-ep);
    }
  }
}


function update() {
  simulate();
  draw();
  if (sim_running) {
    requestAnimationFrame(update);
  }
}


function main() {
  init_particles();
  update();
}

main();
